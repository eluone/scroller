#!/usr/bin/python

import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager

from kivy.config import Config
from kivy.core.window import Window
from kivy.utils import platform


Builder.load_file('screens/game.kv')
Builder.load_file('screens/gameover.kv')
Builder.load_file('screens/about.kv')
Builder.load_file('screens/menu.kv')
Builder.load_file('screens/spacejunkscreenmanager.kv')


# Setup graphics (Stops window resizing on desktop):
Config.set('graphics', 'resizable', 0)
# Uncomment this to show Kivy FPS monitor module:
#Config.set('modules', 'monitor', '')
# Write the above out to kivy.Config
Config.write()

# Graphics fix
Window.clearcolor = (0, 0, 0, 1.0)

# Check if we are running on a desktop and force a portrait 16:9 window
if platform in ('win', 'linux', 'macosx', 'unknown'):
    Window.size = (450, 800)  # Should be a modest size for testing


class SpaceJunkScreenManager(ScreenManager):
    pass


class SpaceJunkScreenManagerApp(App):
    title = 'Space Junk'

    def build(self):
        # return RootWidget()
        return SpaceJunkScreenManager()

    def on_pause(self):
        # Can save data if needed
        # Allow app to be paused on mobile os.
        # http://kivy.org/docs/api-kivy.app.html#pause-mode
        return True

    def on_resume(self):
        # Check if any data needs replacing (usually nothing)
        pass


if __name__ == "__main__":
    SpaceJunkScreenManagerApp().run()
