# Space Junk

A little vertical scrolling game written in Python using the Kivy library

Tested on:
- moto g6 play
/ Using Kivy Launcher v1.9.1.1 (Kivy v1.9.1)
/ Screen Res: 1440 x 720 (18:9)
/ Window Size: 1339 x 720 (Android on-screen controls)
/ Android 8.0.0
- Google Nexus 7 (2013 - Flo)
/ Using Kivy Launcher v1.9.1.1 (Kivy v1.9.1)
/ Screen Res: 1920 x 1200 (16:10)
/ Window Size: 1824 x 1200 (Android on-screen controls)
/ Android 7.1.2 (Lineage OS)
