#!/usr/bin/python

import kivy
kivy.require('1.9.0')

import json

from kivy.uix.screenmanager import Screen
from kivy.clock import Clock


class Menu(Screen):
    def __init__(self, **kwargs):
        super(Menu, self).__init__(**kwargs)

    def on_enter(self):
        # Load local scores if file exists
        try:
            with open('highScoreList.json', 'r') as f:
                self.highScores = json.load(f)
        except:
            # If the file doesn't exist, use default values
            self.highScores = [
                ['Woo', 15000],
                ['TC', 12000],
                ['Woo', 11000],
                ['TC', 10000],
                ['Woo', 9000],
                ]

        # Can't update before widgets displayed:
        Clock.schedule_once(self.updateScores, 0.1)

    def updateScores(self, dt):
        # These are in a Grid Layout (2x5) 
        self.scoreLabel1.text = str(self.highScores[0][1])
        self.scoreLabel1Name.text = str(self.highScores[0][0])
        self.scoreLabel2.text = str(self.highScores[1][1])
        self.scoreLabel2Name.text = str(self.highScores[1][0])
        self.scoreLabel3.text = str(self.highScores[2][1])
        self.scoreLabel3Name.text = str(self.highScores[2][0])
        self.scoreLabel4.text = str(self.highScores[3][1])
        self.scoreLabel4Name.text = str(self.highScores[3][0])
        self.scoreLabel5.text = str(self.highScores[4][1])
        self.scoreLabel5Name.text = str(self.highScores[4][0])

        # Change the high score label on the 'Game' screen to current value
        gameScreenHighScore = self.manager.get_screen('Game').highScoreLabel
        gameScreenHighScore.text = ('High Score: %s %s' % (str(self.highScores[0][1]), str(self.highScores[0][0])))
