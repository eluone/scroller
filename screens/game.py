#!/usr/bin/python

import kivy
kivy.require('1.9.0')

from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.properties import NumericProperty
from kivy.clock import Clock
from kivy.graphics import Rectangle, Color
from kivy.vector import Vector as Vec
from kivy.core.window import Window
from kivy.utils import platform

from kivy.uix.screenmanager import Screen

# Enable logger for debug output while testing.
from kivy.logger import Logger
from timeit import default_timer as timer

from functools import partial
from random import randint
import itertools

# Check if we are running on a desktop and force a portrait 16:9 window
if platform in ('win', 'linux', 'macosx', 'unknown'):
    Window.size = (450, 800)  # Should be a modest size for testing

# Need to scale the movement calculations with device Window size
# Using the g6 window size as a start point (720x1339)
# This will give uniform speed across different screen density / size
# Desktop = 450/720 = 0.625
# Desktop = 800/1440 = 0.555...
screenScaleX = float(Window.width) / 720
screenScaleY = float(Window.height) / 1339  # To account for the android buttons at the bottom

# Using Kivy Logger to check the reported screen size and scale calculation:
Logger.info('App: Game Window %s x %s, Scale %s, %s' % (Window.width, Window.height, screenScaleX, screenScaleY))


class WidgetDrawer(Widget):

    # This widget is used to draw all of the objects on the screen
    # it handles the following:
    # widget movement, size, positioning
    def __init__(self, imageStr, objSize, **kwargs):
        super(WidgetDrawer, self).__init__(**kwargs)

        with self.canvas:
            self.size = objSize
            self.rect_bg = Rectangle(source=imageStr, pos=self.pos, size=self.size)

            self.bind(pos=self.update_graphics_pos)
            self.x = self.center_x
            self.y = self.center_y
            self.pos = (self.x, self.y)
            self.rect_bg.pos = self.pos

    def update_graphics_pos(self, instance, value):
        self.rect_bg.pos = value

    def setSize(self, width, height):
        self.size = (width, height)

    def setPos(self, xpos, ypos):
        self.x = xpos
        self.y = ypos


class Asteroid(WidgetDrawer):
    # Asteroid/Junk class. Dodge these!
    imageStr = ''
    rect_bg = Rectangle(source=imageStr, keep_data=True)  # Try keep_data to keep data after load
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)

    # Randomise the health of each asteroid; used for collisions
    health = randint(3, 7)
    
    # Only allow the bigger junk to split later
    canSplit = True
    canExplode = False
    canCollide = True

    def move(self):
        self.x = self.x + (self.velocity_x * screenScaleX)
        self.y = self.y + (self.velocity_y * screenScaleY)

    def explode(self):
        # Draw explosion for asteroid
        tmpSize = (48 * screenScaleX, 48 * screenScaleY)
        # Image same size as Asteroid/Junk now
        tmpPos = (self.x, self.y)

        # Create an explosion image
        with self.canvas:
            self.explosionRect = Rectangle(source='./images/explosion-1.png', pos=tmpPos, size=tmpSize)

        def changeExplosion(rect, newSource, *largs):
            rect.source = newSource

        # Schedule explosion two
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-2.png'), 0.1)
        # Schedule explosion three
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-3.png'), 0.2)
        # Schedule explosion four
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-4.png'), 0.3)
        # Schedule explosion five
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-5.png'), 0.4)
        # Schedule explosion six
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-6.png'), 0.5)
        # Schedule explosion seven
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-7.png'), 0.6)

        def removeExplosion(rect, *largs):
            self.canvas.remove(rect)
            # Remove the widget after the explosion has been shown

        # Schedule removal
        Clock.schedule_once(partial(removeExplosion, self.explosionRect), 0.7)

    def reset(self):
        # Reuse a defined asteroid as a new one from the top of the screen
        self.health = randint(3, 7)
        self.y = Window.height * 0.99

        # Randomize x start position
        xpos = randint(1, 16)
        xpos = xpos * Window.width * 0.0625
        self.x = xpos

        # Randomize the initial velocity of an asteroid
        vel = randint(75, 100) * screenScaleY
        self.velocity_y = -0.1 * vel

        # Lets add in some possible horizontal movement
        drift = randint(0, 2)

        if drift == 0:
            self.velocity_x = -0.75
        elif drift == 1:
            self.velocity_x = 0
        elif drift == 2:
            self.velocity_x = 0.75

    def update(self):
        self.move()


class Ship(WidgetDrawer):
    # Ship class. This is for the main ship object.
    # Current Image Size: 80x80
    impulse = 3
    grav = -0.1

    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)

    # Ship engine flames tracking list and size
    flames = []
    flameSize = (48 * screenScaleX, 12 * screenScaleY)  # Same ratio as ship model

    # Movement boundaries:
    playAreaTop = Window.height * 0.95
    playAreaBottom = Window.height * 0.05
    playAreaLeft = 0
    playAreaRight = Window.width - (48 * screenScaleX)  # Width - Ship width

    def move(self):
        # Don't let the ship go off the bottom of the screen:
        if self.y < self.playAreaBottom:
            # Push the ship upwards
            self.impulse = 1
            self.grav = -0.1

        # Don't let the ship go too far up or across the screen:
        if self.y > self.playAreaTop:
            self.impulse = -3

        if self.x < self.playAreaLeft:
            self.velocity_x = 0.5
        elif self.x > self.playAreaRight:
            self.velocity_x = -0.5

        # Move the ship
        self.x = self.x + (self.velocity_x * screenScaleX)
        self.y = self.y + (self.velocity_y * screenScaleY)

        # Move the ships engine flames at the same time 
        for x in self.flames:
            x.pos = (self.pos[0], self.pos[1] - self.flameSize[1])

    def determineVelocity(self):
        # Move the ship
        # Need to take into account our acceleration and gravity
        self.grav = self.grav * 1.05  # increase gravity
        # Set the grav limit
        if self.grav < -4:
            self.grav = -4

        # the ship has a property called self.impulse which is updated
        # whenever the player touches, pushing the ship up
        # use this impulse to determine the ship velocity
        # also decrease the magnitude of the impulse each time its used
        self.velocity_y = self.impulse + self.grav
        self.impulse = 0.95 * self.impulse

        # Decrease x velocity each update
        self.velocity_x = 0.9 * self.velocity_x

    def drawFlame(self, *largs):
        # Draw the arrows directly onto the canvas
        with self.canvas:
            flamePos = (self.pos[0], self.pos[1] - self.flameSize[1])
            flameRect = Rectangle(source='./images/flames.png', pos=flamePos, size=self.flameSize)
            self.flames.append(flameRect)

        # Do removal of flames (From canvas and list)
        def removeFlames(flame, *largs):
            self.canvas.remove(flame)
            self.flames.remove(flame)

        # Schedule the flames to be removed
        Clock.schedule_once(partial(removeFlames, flameRect), 0.2)

    def explode(self):
        # Draw Explosion for ship
        # Current Image Size: 20x20
        tmpSize = (48 * screenScaleX, 48 * screenScaleY)
        # Image same size as Ship now
        tmpPos = (self.x, self.y)

        # Create an explosion image
        with self.canvas:
            self.explosionRect = Rectangle(source='./images/explosion-1.png', pos=tmpPos, size=tmpSize)

        def changeExplosion(rect, newSource, *largs):
            rect.source = newSource

        # Schedule explosion two
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-2.png'), 0.1)
        # Schedule explosion three
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-3.png'), 0.2)
        # Schedule explosion four
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-4.png'), 0.3)
        # Schedule explosion five
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-5.png'), 0.4)
        # Schedule explosion six
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-6.png'), 0.5)
        # Schedule explosion seven
        Clock.schedule_once(partial(changeExplosion, self.explosionRect, './images/explosion-7.png'), 0.6)

        def removeExplosion(rect, *largs):
            self.canvas.remove(rect)

        # Schedule removal
        Clock.schedule_once(partial(removeExplosion, self.explosionRect), 0.7)

    def update(self):
        self.determineVelocity()
        self.move()


class Bullet(WidgetDrawer):
    # Bullet class. Fire back!
    imageStr = './images/bullet.png'

    rect_bg = Rectangle(source=imageStr, keep_data=True)  # Try keep_data to keep data after load
    velocity_y = NumericProperty(0)

    def move(self):
        self.y = self.y + (self.velocity_y * screenScaleY)

    def update(self):
        self.move()


class GUI(Widget):
    # This is the main widget that contains the game.
    # Generate list of Asteroid/Junk widgets for tracking & collision detection
    asteroidList = []
    # Generate list of bullet widgets for tracking & collision detection
    bulletList = []

    # numericproperty used so we can bind a callback
    # to use every time the number changes
    asteroidScore = NumericProperty(0)

    # This is part of the spawn probability: if randint(1,2000) > minProb
    # minProb is decreases by 1 after each spawn down to minimum of 1000
    minProb = 1900

    # Setup boundaries for off screen so it isn't calculated per loop
    rightBoundary = Window.width + 10
    topBoundary = Window.height + 10

    # Split the screen into thirds to change touch responses
    thirdArea = Window.width / 3

    def __init__(self, **kwargs):
        super(GUI, self).__init__(**kwargs)

        def check_score(self, obj):
            # Update score label widget with current score.
            # The scoreLabel widget now belongs to the parent of GUI as it is in the kv file: (Screen)
            self.parent.scoreLabel.text = 'Dodged: ' + str(self.asteroidScore)

        self.bind(asteroidScore=check_score)  # This will update the score widget when asteroidScore changes

        # Now we create a ship object
        shipSize = (48 * screenScaleX, 48 * screenScaleY)
        self.ship = Ship('./images/ship.png', shipSize)
        self.ship.x = Window.width / 2 - (self.ship.width / 2)
        self.ship.y = Window.height / 8
        self.add_widget(self.ship)

    def addAsteroid(self):
        # Pick a random asteroid image
        imageNumber = randint(1, 6)
        imageStr = './images/junk_' + str(imageNumber) + '.png'
        asteroidSize = (48 * screenScaleX, 48 * screenScaleY)
        tmpAsteroid = Asteroid(imageStr, asteroidSize)
        tmpAsteroid.y = Window.height * 0.99

        # Randomize x start position
        xpos = randint(1, 100) / 100
        tmpAsteroid.x = Window.width * xpos
        
        # Randomize the initial velocity of an asteroid
        vel = randint(75, 100) * screenScaleY
        tmpAsteroid.velocity_y = -0.1 * vel

        # Lets add in some possible horizontal movement
        tmpAsteroid.velocity_x = 0
        drift = randint(0, 2)

        if drift == 0:
            tmpAsteroid.velocity_x = -0.75
        elif drift == 1:
            tmpAsteroid.velocity_x = 0
        elif drift == 2:
            tmpAsteroid.velocity_x = 0.75

        # Add to the list of junk and add to the screen
        self.asteroidList.append(tmpAsteroid)
        self.add_widget(tmpAsteroid)

    def addSmallAsteroid(self, imageStr, startX, startY, velX, velY):
        # Inherit big junk image so we get smaller version of source
        asteroidSize = (24 * screenScaleX, 24 * screenScaleY)
        tmpAsteroid = Asteroid(imageStr, asteroidSize)
        tmpAsteroid.x = startX
        tmpAsteroid.y = startY
        tmpAsteroid.velocity_x = velX
        tmpAsteroid.velocity_y = velY
        # Limit the small junk items health and don't allow them to split again
        tmpAsteroid.health = 2
        tmpAsteroid.canSplit = False
        # Add to the list of junk and add to the screen
        self.asteroidList.append(tmpAsteroid)
        self.add_widget(tmpAsteroid)

    def addBullet(self):
        # Add a bullet starting from just above the ship
        imageStr = './images/bullet.png'
        size = (12 * screenScaleX, 12 * screenScaleY)
        tmpBullet = Bullet(imageStr, size)
        
        tmpBullet.y = self.ship.y + self.ship.height
        tmpBullet.x = self.ship.x + (self.ship.width / 2) - (tmpBullet.width / 2)

        tmpBullet.velocity_y = 10 * screenScaleY

        # Add to the list of asteroids and add to the screen
        self.bulletList.append(tmpBullet)
        self.add_widget(tmpBullet)

    def drawTouchResponse(self, direction, x, y):
        # Draw the touch arrows directly onto the canvas
        with self.canvas:
            tmpSize = 48 * screenScaleX, 48 * screenScaleY
            tmpPos = (x - tmpSize[0] / 4, y - tmpSize[1] / 4)
            self.arrowRect = Rectangle(source=('./images/' + direction + '1.png'), pos=tmpPos, size=tmpSize)

        def changeArrow(rect, newSource, *largs):
            rect.source = newSource

        # Schedule touch arrows two
        Clock.schedule_once(partial(changeArrow, self.arrowRect, ('./images/' + direction + '2.png')), 0.1)
        # Schedule touch arrows three
        Clock.schedule_once(partial(changeArrow, self.arrowRect, ('./images/' + direction + '3.png')), 0.2)
        # Schedule touch arrows four
        Clock.schedule_once(partial(changeArrow, self.arrowRect, ('./images/' + direction + '4.png')), 0.3)

        def removeArrows(arrow, *largs):
            self.canvas.remove(arrow)

        # Schedule removal
        Clock.schedule_once(partial(removeArrows, self.arrowRect), 0.4)

    # Handle touch input events
    def on_touch_down(self, touch):
        if touch.x < self.thirdArea:  # Left third of screen
            self.drawTouchResponse('left', touch.x, touch.y)
            self.ship.impulse = 1.5 * screenScaleY
            self.ship.velocity_x = -8 * screenScaleX
            self.ship.grav = -0.1
            self.ship.drawFlame()
        elif touch.x > self.thirdArea * 2:  # Right third of screen
            self.drawTouchResponse('right', touch.x, touch.y)
            self.ship.impulse = 1.5 * screenScaleY
            self.ship.velocity_x = 8 * screenScaleX
            self.ship.grav = -0.1
            self.ship.drawFlame()
        elif touch.y > self.thirdArea:  # Middle area above the fire button
            self.drawTouchResponse('up', touch.x, touch.y)
            self.ship.impulse = 5 * screenScaleY  # Up only
            self.ship.grav = -0.1
            self.ship.drawFlame()
        else:  # Should be on the fire button area
            self.addBullet()
            # Don't draw touch response arrows
            # Don't move the ship
            # Don't reset the gravity
            # Don't draw the flames

    def update(self, dt):
        # This update function is the main update function for the game
        # All of the game logic and events are setup here as well

        # Update ship
        self.ship.update()

        # Update asteroids & check if we can randomly add an asteroid
        # self.asteroidScore  # How many we've passed
        numAsteroids = len(self.asteroidList)  # How many asteroids on screen
        maxAsteroids = 5 + int(self.asteroidScore / 10)  # Limit number of asteroids on screen: 5 +1 per 10 scored

        # A few rules to stagger the number of objects thrown at the player:
        # Only try to generate an object if:
        # 1. There are none on screen
        # 2. Not at the currently max allowed
        if (numAsteroids == 0) or (numAsteroids < maxAsteroids):
            tmpCount = randint(1, 2000)
            if tmpCount > self.minProb:
                self.addAsteroid()
                if self.minProb > 1000:
                    # Decrease the minProb by 1 per asteroid spawned
                    self.minProb = self.minProb - 1

        # Collect indexes of list items to be removed later:
        cleanUpAsteroidList = []
        cleanUpBulletList = []
        addAsteroidList = []

        t0 = timer()

        for asteroid in self.asteroidList:
            # Check for collisions with the ship
            if asteroid.collide_widget(self.ship):
                # Game over routine
                # Stop the update clock
                Clock.unschedule(self.update)
                self.ship.explode()
                # Change to the Game Over screen
                self.parent.manager.current = 'GameOver'
            elif (asteroid.y < -10):  # Most likely to be True
                # Add to score if goes off bottom; otherwise just remove it.
                self.asteroidScore = self.asteroidScore + 1
                # Add to list to be removed outside loop
                cleanUpAsteroidList.append(self.asteroidList.index(asteroid))
            elif (asteroid.y > self.topBoundary) or (asteroid.x < -10) or (asteroid.x > self.rightBoundary):
                # Add to list to be removed outside loop
                cleanUpAsteroidList.append(self.asteroidList.index(asteroid))

        asteroidsShipBoundries = timer() - t0

        # Use the list of bullets to check for collisions with Asteroids/Junk:
        # We will remove 2 health from the junk each time here (so junk.health may be less than 0)
        for bulletIndex, b in enumerate(self.bulletList):
            # Check to see if bullet is off of screen first
            if (b.y > self.topBoundary):
                # Add to list to be removed outside loop
                cleanUpBulletList.append(bulletIndex)
            else:
                for asteroidIndex, a in enumerate(self.asteroidList):
                    # Check if we have already marked the bullet or Junk for removal
                    if (bulletIndex not in cleanUpBulletList) and (asteroidIndex not in cleanUpAsteroidList):
                        if b.collide_widget(a):
                            # Decrease the health of object by 2:
                            a.health = a.health - 2
                            # Add to list to be removed outside loop
                            cleanUpBulletList.append(bulletIndex)
                            # See if the asteroid/junk has no health left and explode it:
                            if a.health <= 0:
                                a.canExplode = True
                                if (a.canSplit == True) and (numAsteroids < maxAsteroids):
                                    col_vector = Vec(a.pos) - Vec(b.pos)
                                    col_vector_mag = col_vector.length()
                                    # Might need to check this calc?
                                    a.velocity_x, a.velocity_y = 4.0 / col_vector_mag * col_vector
                                    b.velocity_x, b.velocity_y = -4.0 / col_vector_mag * col_vector
                                    addAsteroidList.append([a.rect_bg.source, a.x, a.y, a.velocity_x, a.velocity_y])
                                    addAsteroidList.append([a.rect_bg.source, b.x, b.y, b.velocity_x, b.velocity_y])
                                # Add to list to be removed outside loop
                                cleanUpAsteroidList.append(asteroidIndex)

        asteroidsVsBulletsTime = timer() - t0

        # Check for Asteroid/Junk collisions and bounce them off each other:
        # Comparing a list with itself using combinations
        for l, m in itertools.combinations(self.asteroidList, 2):
            lIndex = self.asteroidList.index(l)
            mIndex = self.asteroidList.index(m)
            # Check if we have already marked the Asteroid/Junk for removal
            if (lIndex not in cleanUpAsteroidList) and (mIndex not in cleanUpAsteroidList):
                if l.collide_widget(m):
                    col_vector = Vec(l.pos) - Vec(m.pos)
                    col_vector_mag = col_vector.length()
                    # Small asteroids spawn on the old large asteroids position
                    # We need to exclude them from the collisions as the col_vector_mag will be 0
                    if col_vector_mag > 24:
                        # Might need to check this calc?
                        l.velocity_x, l.velocity_y = 4.0 / col_vector_mag * col_vector
                        m.velocity_x, m.velocity_y = -4.0 / col_vector_mag * col_vector
                        # Decrease the health of each object by 1:
                        l.health = l.health - 1
                        m.health = m.health - 1
                        # See if either of the objects have no health left and explode them:
                        if l.health == 0:
                            l.canExplode = True
                            if l.canSplit == True:
                                addAsteroidList.append([l.rect_bg.source, l.x, l.y, l.velocity_x, l.velocity_y])
                                addAsteroidList.append([l.rect_bg.source, m.x, m.y, m.velocity_x, m.velocity_y])
                            # Remove the widget from the list to be tracked
                            cleanUpAsteroidList.append(lIndex)
                        if m.health == 0:
                            m.canExplode = True
                            if m.canSplit == True:
                                addAsteroidList.append([m.rect_bg.source, l.x, l.y, l.velocity_x, l.velocity_y])
                                addAsteroidList.append([m.rect_bg.source, m.x, m.y, m.velocity_x, m.velocity_y])
                            # Remove the widget from the list to be tracked
                            cleanUpAsteroidList.append(mIndex)

        combinationsTime = timer() - t0

        def scheduledRemoveJunk(item, dt):
            self.remove_widget(item)

        # Clean up junk after loops above so that we are not modifying the list we are looping through
        # Go through the indexes in descending order so we remove the highest first
        for x in sorted(cleanUpAsteroidList, reverse=True):
            if self.asteroidList[x].canExplode == True:
                self.asteroidList[x].explode()
                # Remove after explosion animation:
                #Clock.schedule_once(partial(scheduledRemoveJunk, self.asteroidList[x]), 0.8)
                #del self.asteroidList[x]
                self.asteroidList[x].reset()
            else:  # Gone out of bounds
                #self.remove_widget(self.asteroidList[x])
                #del self.asteroidList[x]
                self.asteroidList[x].reset()

        for y in sorted(cleanUpBulletList, reverse=True):
            self.remove_widget(self.bulletList[y])
            del self.bulletList[y]

        cleanUpTime = timer() - t0

        # Now we can add the queued Asteroids / Junk
        for z in addAsteroidList:
            # For reference: addSmallAsteroid(imageStr, startX, startY, velX, velY)
            self.addSmallAsteroid(z[0], z[1], z[2], z[3], z[4])

        addAsteroidTime = timer() - t0

        def scheduledUpdateJunk(item, dt):
            item.update()

        # Update bullets and junk after collisions / removals / additions
        # Using clock to schedule updates outside of logic loop
        for junk in self.asteroidList:
            Clock.schedule_once(partial(scheduledUpdateJunk, junk))
        for bullet in self.bulletList:
            Clock.schedule_once(partial(scheduledUpdateJunk, bullet))

        # Check for slow updates in this loop and log timings
        elapsedTime = timer() - t0
        if elapsedTime > 0.02:
            Logger.info('Perf: %s Objects, JvS/OB: %f, JvB: %f, AvA: %f, Clean: %f, Add Small: %f, Total: %f' % 
                        (len(self.asteroidList),
                         asteroidsShipBoundries,
                         (asteroidsVsBulletsTime - asteroidsShipBoundries),
                         combinationsTime- asteroidsVsBulletsTime,
                         cleanUpTime - combinationsTime,
                         addAsteroidTime - cleanUpTime,
                         elapsedTime))

class Game(Screen):

    def __init__(self, **kwargs):
        super(Game, self).__init__(**kwargs)

    def on_enter(self):
        # Add the game widgets after load of kv file (only on first use)
        # This is to make sure the python added widgets are on top.
        # This will be the ship & junk images
        if len(self.children) == 7:  # This is the number of widgets in the kv file (7)
            self.app = GUI()
            self.add_widget(self.app)
        # Start the game when changed to this screen
        Clock.schedule_interval(self.app.update, 1.0 / 60.0)

    def on_pre_leave(self):
        # Stop the game when changing away from this screen
        Clock.unschedule(self.app.update)

