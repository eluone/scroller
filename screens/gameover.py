#!/usr/bin/python

import kivy
kivy.require('1.9.0')

from operator import itemgetter
import json

from kivy.uix.screenmanager import Screen
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.textinput import TextInput


class GameOver(Screen):
    def __init__(self, **kwargs):
        super(GameOver, self).__init__(**kwargs)
        # Load local scores if file exists
        try:
            with open('highScoreList.json', 'r') as f:
                self.highScores = json.load(f)
        except:
            # If the file doesn't exist, use default values
            self.highScores = [
                ['Woo', 15000],
                ['TC', 12000],
                ['Woo', 11000],
                ['TC', 10000],
                ['Woo', 9000],
                ]

    def on_pre_enter(self):
        # Reset the starting score for the counter
        self.currentScore = 0

    def on_enter(self):
        # Get the score from the game screen
        self.finalScore = self.manager.get_screen('Game').app.asteroidScore * 100
        Clock.schedule_once(self.updateScore, 0.02)

    def on_pre_leave(self):
        # Save the high scores to file with the inputed text
        playerName = self.scoreTextInput.text

        # Add the players score to the list
        self.highScores.append([playerName, self.finalScore])
        # Sort the scores and only keep the top 5
        self.highScores = sorted(self.highScores, key = itemgetter(1), reverse = True)[:5]
        
        with open('highScoreList.json', 'w') as f:
            json.dump(self.highScores, f)

        # Define the game screen:
        game = self.manager.get_screen('Game').app

        # Remove all of the Asteroids / Junk
        for k in game.asteroidList:
            game.remove_widget(k)
        game.asteroidList = []

        # Remove all of the Bullets
        for l in game.bulletList:
            game.remove_widget(l)
        game.bulletList = []

        # Reset ship to start position and reset velocity
        game.ship.x = Window.width / 2 - (game.ship.width / 2)
        game.ship.y = Window.height / 8
        game.ship.velocity_x = 0
        game.ship.velocity_y = 0

        # Reset the spawn probability and score
        game.minProb = 1900
        game.asteroidScore = 0

        # Change the high score label on the 'Game' screen to current value
        gameScreenHighScore = self.manager.get_screen('Game').highScoreLabel
        gameScreenHighScore.text = ('High Score: %s %s' % (str(self.highScores[0][1]), str(self.highScores[0][0])))

    def updateScore(self, dt):
        if self.finalScore > 0:
            self.currentScore = self.currentScore + 100
        self.scoreLabel.text = 'Score: ' + str(self.currentScore)

        # 0-10 asteroids 0 stars
        # 11-50 asteroids 1 star / >= 1000
        # 51-200 asteroids 2 stars / >= 5000
        # 201-500 asteroids 3 stars / >= 20000
        # 501-1000 asteroids 4 stars / >= 50000
        # 1001+ asteroids 5 stars / >= 100000
        starTriggers = [1000, 5000, 20000, 50000, 100000]
        if self.currentScore in starTriggers:  # Only call at each gold star minimum score
            self.updateStars()  # Update the number of stars with the current score

        if self.currentScore < self.finalScore:  # Schedule loop until finalScore reached
            Clock.schedule_once(self.updateScore, 0.02)

    def updateStars(self):
        # Update each star to gold as the score passes the required value.
        starNumber = 0

        if self.currentScore >= 1000:
            starNumber = 1
        if self.currentScore >= 5000:
            starNumber = 2
        if self.currentScore >= 20000:
            starNumber = 3
        if self.currentScore >= 50000:
            starNumber = 4
        if self.currentScore >= 100000:
            starNumber = 5

        with self.canvas:
            # star one
            if starNumber == 1:
                self.starOne.source = './images/gold_star.png'
            # star two
            if starNumber == 2:
                self.starTwo.source = './images/gold_star.png'
            # star three
            if starNumber == 3:
                self.starThree.source = './images/gold_star.png'
            # star four
            if starNumber == 4:
                self.starFour.source = './images/gold_star.png'
            # star five
            if starNumber == 5:
                self.starFive.source = './images/gold_star.png'


class InitialsInput(TextInput):
    # Only allow 3 characters and prefer capitals
    def insert_text(self, substring, from_undo=False):
        if len(substring) > 2:  # Input from Android is entered as one string
            s = substring[:3].upper()
        elif len(self.text) > 2:  # Desktop appears to update the box per character
            s = ''
        else:
            s = substring.upper()

        return super(InitialsInput, self).insert_text(s, from_undo=from_undo)

